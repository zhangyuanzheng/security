/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50519
Source Host           : localhost:3306
Source Database       : security02

Target Server Type    : MYSQL
Target Server Version : 50519
File Encoding         : 65001

Date: 2019-10-28 10:00:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of persistent_logins
-- ----------------------------
INSERT INTO `persistent_logins` VALUES ('d', 'eoD+qErQWs0TB8CwczPQKQ==', 'H0EXXeAHNvniYgbTlwBQFQ==', '2019-10-28 09:48:09');
INSERT INTO `persistent_logins` VALUES ('demo', 'TYM9LkKQGkgG7QUtYJyzZQ==', 'vqfp61tEJ0PeGouDeh5Ngg==', '2019-10-24 11:11:31');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'ROLE_ADMIN');
INSERT INTO `sys_role` VALUES ('2', 'ROLE_USER');
INSERT INTO `sys_role` VALUES ('3', 'ROLE_TEACHER');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('13', '123', '$2a$10$24jrSR9nljXM7MBnKXdzqepYJyiqhXMN1HWEoIdH1BlesTdzBnugK');
INSERT INTO `sys_user` VALUES ('14', 'a', '$2a$10$mybC43ls/latHZVgk5VccuwGEfw8avqrXt1nI795fuHhLQv/RnhYa');
INSERT INTO `sys_user` VALUES ('15', 'b', '$2a$10$NEhkRIPas3UgJdVG5/DzFOfN74EWUbTkqnhCRL4ktgCxhulli9mXG');
INSERT INTO `sys_user` VALUES ('17', 'demo', '$2a$10$LK2pZEY.sg.lJvcO8mmHLO2Y0epflAS3B7F5UjKZWHfHnjTYQo6L.');
INSERT INTO `sys_user` VALUES ('18', 'hhh', '$2a$10$YF6Z5omAd/rk4pI83MIyCe//0VnljseNyN/7cCG9krAOYVKQ.JXn6');
INSERT INTO `sys_user` VALUES ('19', 'c', '$2a$10$cVXTVUStWEQNJ1Pt3r9VaOHU7LA4ModjOAszvTnUm5ffaHGAD6Isa');
INSERT INTO `sys_user` VALUES ('20', 'd', '$2a$10$sGrQdhCVj3xybzXl1it/H.PPsBtEcjJCK8EbLjcXrC9Tp7gB0YJkC');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE,
  KEY `fk_role_id` (`role_id`) USING BTREE,
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('13', '1');
INSERT INTO `sys_user_role` VALUES ('14', '1');
INSERT INTO `sys_user_role` VALUES ('15', '1');
INSERT INTO `sys_user_role` VALUES ('17', '1');
INSERT INTO `sys_user_role` VALUES ('18', '2');
INSERT INTO `sys_user_role` VALUES ('19', '3');
INSERT INTO `sys_user_role` VALUES ('20', '3');
